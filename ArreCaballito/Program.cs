﻿using System;
using System.Threading;

namespace ArreCaballito
{
    /*--- Classes ---*/
    public class Cavallo
    {
        /*--- Propedades ---*/
        public string Nombre { get; set; }
        public int Posicion { get; set; }
        public int Velocidad { get; set; }

        /*--- Constructor ---*/
        public Cavallo(string nombre)
        {
            Nombre = nombre;
            Posicion = 0;
            Velocidad = new Random().Next(1, 5);
        }
        /*--- Metodos ---*/
        public void Avanzar(int pos)
        {
            for (int i = 0; i < Velocidad; i++)
            {
                if (Posicion + Nombre.Length < Console.BufferWidth)
                {
                    Console.SetCursorPosition(Nombre.Length + Posicion, pos);
                    Console.Write('*');
                    Posicion++;
                }
            }
            Thread.Sleep(new Random().Next(1000, 1500));
        }
    }

    public class Carrera
    {
        /*--- Propedades ---*/
        public Cavallo[] Cavallos;
        public int Meta;
        public bool Juego { get; set; }

        /*--- Constructor ---*/
        public Carrera()
        {
            Cavallos = new Cavallo[6];
            for (int i = 0; i < Cavallos.Length; i++)
            {
                Cavallos[i] = new Cavallo("Cavallo " + (i + 1));
            }
            Meta = Console.BufferWidth - Cavallos[0].Nombre.Length;
            Juego = false;
        }
        /*--- Metodos ---*/
        public bool Final()
        {
            foreach (Cavallo cavallo in Cavallos)
            {
                if (cavallo.Posicion >= Meta)
                {
                    Console.SetCursorPosition(0, 8);
                    Console.WriteLine($"\n A gunyat {cavallo.Nombre}");
                    Juego = true;
                    return false;
                }
            }
            return true;
        }
        public void JugarCarrera()
        {
            Thread thread1 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 1);
                Console.Write(Cavallos[0].Nombre);
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[0].Avanzar(1);
                }
            });
            Thread thread2 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 2);
                Console.Write(Cavallos[1].Nombre);
                thread1.Start();
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[1].Avanzar(2);
                }
            });
            Thread thread3 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 3);
                Console.Write(Cavallos[2].Nombre);
                thread2.Start();
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[2].Avanzar(3);
                }
            });
            Thread thread4 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 4);
                Console.Write(Cavallos[3].Nombre);
                thread3.Start();
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[3].Avanzar(4);
                }
            });
            Thread thread5 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 5);
                Console.Write(Cavallos[4].Nombre);
                thread4.Start();
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[4].Avanzar(5);
                }
            });
            Thread thread6 = new Thread(() =>
            {
                Thread.Sleep(1000);
                Console.SetCursorPosition(0, 6);
                Console.Write(Cavallos[5].Nombre);
                thread5.Start();
                Thread.Sleep(new Random().Next(1000, 1500));
                while (Final())
                {
                    Cavallos[5].Avanzar(6);
                }
            });
            thread6.Start();
            if (Juego)
            {
                thread1.Abort(); thread2.Abort(); thread3.Abort(); thread4.Abort(); thread5.Abort(); thread6.Abort();
            }
        }
    }

    class Program
    {
        static void Main()
        {
            Carrera carrera = new Carrera();
            while ((AskString("Prem 'C' per comencar").ToLower()) != "c")
            {
            }
            Console.Clear();
            carrera.JugarCarrera();
        }
        public static string AskString(string pregunta)
        {
            Console.WriteLine(pregunta);
            return Console.ReadLine();
        }
    }
}
